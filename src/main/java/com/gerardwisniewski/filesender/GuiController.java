package com.gerardwisniewski.filesender;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GuiController implements Initializable {
    @FXML private Label statusLabel;

    @FXML private TextField fileNameText;
    @FXML private Button selectFileAction;

    @FXML private Button uploadButton;

    @FXML private TextField addressText;
    @FXML private TextField portText;

    @FXML private ProgressBar progressBar;

    private ExecutorService executor = Executors.newSingleThreadExecutor();

    @FXML
    private void handleSelectFileAction() {
        File file = new FileChooser().showOpenDialog(null);
        if (file == null){
            return;
        }
        fileNameText.setText(file.getAbsolutePath() );
    }
    
    @FXML
    private void handleButtonAction() {
        File file = new File(fileNameText.getText());

        if (!file.exists())
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("File not found");
            alert.setContentText("Path doesn't exist");
            alert.show();
            return;
        }

        else if (fileNameText.getText().equals(""))
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("File not selected");
            alert.setContentText("No file selected");
            alert.show();
            return;
        }

        Task<Void> task = new SendTask(file, addressText.getText(), Integer.parseInt(portText.getText()));
        statusLabel.textProperty().bind(task.messageProperty());

        fileNameText.disableProperty().bind(task.runningProperty());
        selectFileAction.disableProperty().bind(task.runningProperty());

        uploadButton.disableProperty().bind(task.runningProperty());

        portText.disableProperty().bind(task.runningProperty());
        addressText.disableProperty().bind(task.runningProperty());

        progressBar.progressProperty().bind(task.progressProperty());

        executor.submit(task);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        portText.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                portText.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
    }    
    
}

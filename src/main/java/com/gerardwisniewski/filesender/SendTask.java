package com.gerardwisniewski.filesender;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;

import javafx.concurrent.Task;

public class SendTask extends Task<Void> {
    private File fileToUpload;
    private String address;
    private int port;
    
    SendTask(File file, String address, int port) {
        this.address = address;
        this.port = port;
        fileToUpload = file;
    }
    
    @Override
    protected Void call() {
        updateMessage("Connecting to server...");
        updateProgress(0, 100);

        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(address, port), 2000);
            socket.setSoTimeout(1000);
            if (!socket.isConnected()) {
                updateMessage("ERROR: Failed to connect to the server.");
                return null;
            }

            try (DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream())) {
                long totalBytes = fileToUpload.length();
                long totalRead = 0;

                dataOutputStream.writeUTF(fileToUpload.getName());
                dataOutputStream.writeLong(totalBytes);

                updateMessage("Sending...");
                updateProgress(0, totalBytes);

                try (FileInputStream fileInputStream = new FileInputStream(fileToUpload)) {
                    byte[] buffer = new byte[2048];
                    int read;
                    while ((read = fileInputStream.read(buffer)) != -1) {
                        dataOutputStream.write(buffer, 0, read);
                        totalRead += read;
                        updateProgress(totalRead, totalBytes);
                    }
                }
            }
        } catch (SocketTimeoutException e) {
            updateMessage("ERROR: Timeout while connecting to socket");
            return null;
        }
          catch (IOException e) {
            updateMessage("Unknown ERROR: " + e.getMessage());
            return null;
        }

        updateMessage("Finished uploading file");
        updateProgress(100, 100);
        
        return null;
    }
}
